// Andres Recalde
// arecald1

#include <stdio.h>

int main(void) {
printf("Please enter an arithmetic expression using * and / only: \n");

float num = 0.0f;
float result = 0.0f;
char operator = '?';
int error = 0; // holds a value that indicates error type  
char mal[] = "malformed expression";
char div_by_0[] = "division by zero";
int entered = 0; // tracks whether the while loop was entered

	
	//  Loops while there is a float to read in and no errors triggered
	while((scanf(" %f", &num) == 1) && error == 0) {
		entered = 1; // confirms loop has been answered
		
		
		// First checks for division by zero error, then checks for which math operation to perform, 
		// if no character is provided its the beginning of the input and the number will be stored in result.
		
		if (operator == '/' && num == 0.0) {
			error = 2;
			break;
		} else if (operator == '*') {
			result = result * num;
		} else if (operator == '/') {
			result = result / num;
		} else {
			result = num;
		}
		
		// checks if next character is valid character
		if(scanf(" %f", &num) == 1) {
			error = 1;
			printf("\n%s\n", mal);
			return(1);
		} else if(scanf(" %c", &operator) == 1) {
			if (operator != '*' && operator != '/') {
				error = 1;
			}
		}	
	}
	
	// determines if an error occured or prints the result of the computation if no errors were triggered
	if (error == 2) { // if error is two a division by zero error has occured
		printf("\n%s\n", div_by_0);
		return(2);
	} else if (scanf(" %c", &operator) == 1 || error == 1 || entered == 0) { // checks if there is a character after the while loop ends, if there is a malformed expression, or if the loop was ever entered
		printf("\n%s\n", mal);
		return(1);
	} else { // prints result of computation if no errors were reached
		printf("\n%f\n", result);
	}	


return 0;
}
 
