// Andres Recalde
// arecald1

#include <stdio.h>

int main(void) {
printf("Please enter an arithmetic expression using * and / only: \n");

float num = 0.0f;
float result = 0.0f;
char operator = '?';
int error = 0; // holds a value that indicates error type  
char mal[] = "malformed expression";
char div_by_0[] = "division by zero";
int entered = 0; // tracks whether the while loop was entered to obtain a float

	
	//  Loops while there is a float to read in and no errors triggered
	while((scanf(" %f", &num) == 1) && error == 0) {
		entered = 1; // confirms loop has been answered
		
		
		// First checks for division by zero error, then checks for which math operation to perform, 
		// if no character is provided its the beginning of the input and the number will be stored in result
		if (operator == '/' && num == 0.0) {
			error = 2;
			printf("%s\n", div_by_0);
			return(error);
		} else if (operator == '*') {
			result = result * num;
		} else if (operator == '/') {
			result = result / num;
		} else {
			result = num;
		}
		
		// Checks if there are two floats back to back then,
		// checks if next character is valid character
		if(scanf(" %f", &num) == 1) {
			error = 1;
			printf("%s\n", mal);
			return(error);
		} else if(scanf(" %c", &operator) == 1) {
			entered = 0; //to make sure that if an operator is used there is a float after it
			if (operator != '*' && operator != '/') {
				error = 1;
				printf("%s\n", mal);
				return(error);
			}
		}	
	}
	
	// Checks if the loop has been entered to ensure the expression ends in a float
	// and checks if the expression ends in a character
	if (scanf(" %c", &operator) == 1 || entered == 0) {
		printf("%s\n", mal);
		return(1);
	} else { // prints result of computation if no errors were reached
		printf("%f\n", result);
	}	


return 0;
}
 
