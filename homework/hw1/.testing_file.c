// Andres Recalde
// arecald1

#include <stdio.h>

int main(void) {
printf("Please enter an arithmetic expression using * and / only: \n");

float num = 0.0f;
//char operator_raw = '0';
//float prev = 0.0f;
float result = 0.0f;
char operator = '?';
int error = 0;
char mal[] = "malformed expression";
char div_by_0[] = "division by zero";
//char input = '0';
//char equation[] = ""; 
//int index = 0;

	while((scanf(" %f", &num) == 1) && error == 0) { 
		if (operator == '/' && num == 0.0) {
			error = 2;
		} else if (operator == '*') {
			result = result * num;
		} else if (operator == '/') {
			result = result / num;
		} else {
			result = num;
		}


		if(scanf(" %c", &operator) == 1) {  
			if (operator != '*' && operator != '/') {
				error = 1;
			}		
		}
	}

	
	if (scanf(" %c", &operator) == 1) {
		printf("\n%s\n", mal);
	} else if (error == 1) {
		printf("\n%s\n", mal);
	} else if (error == 2) {
		printf("\n%s\n", div_by_0);
	
	} else {
		printf("\n%f\n", result);
	}	


return 0;
} //THIS IS WHEN I REALIZED THE GENERAL SOLUTION, FURTHER EDITS WERE MADE IN FINAL FILE FOR PERFECTION
