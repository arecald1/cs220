//Andres Recalde
//arecald1

#include "CTree.h"
#include <iostream>
#include <sstream>
#include <string>
using std::string;
using std::stringstream;
using std::ostream;

// Properly instantiates a new root node
CTree::CTree(char ch) : data(ch), kids(nullptr), sibs(nullptr), prev(nullptr) {}

// Destructor that deletes all child nodes and then all sibling node
CTree::~CTree() {
	if (kids != nullptr) {
		delete kids;
	}
	if (sibs != nullptr) {
		delete sibs;
	}
}

// Overrides the ^ operator to insert a root node into the tree as a child
CTree& CTree::operator^(CTree& rt) {
	this->addChild(&rt);
	return *this;
}

// Overrides the == operator to recursively traverse the tree to ensure that
// every node value and the structure is identical
bool CTree::operator==(const CTree &root) {
	// Checks if the root nodes have the same data
	if (!(data == root.data)) {
		return false;
	}
	
	bool is_kid_null = kids != nullptr;
	bool is_r_kid_null = root.kids != nullptr;
	
	// Checks if both nodes have a child node
	if (!(is_kid_null == is_r_kid_null)) {
		return false;
	}
	
	// Checks if both nodes have a child node, then recursively calls the 
	// == operator on the child nodes
	if (is_kid_null && is_r_kid_null) {
		if (!(*kids == *(root.kids))) {
			return false;
		}
	}
	

	bool is_sibs_null = sibs != nullptr;
	bool is_r_sibs_null = root.sibs != nullptr;
	 
	// Checks if both nodes have a sibling node
	if (!(is_sibs_null == is_r_sibs_null)) {
		return false;
	}
	
	// Checks if both nodes have a child node, then recursively calls the
	// == operator on the sibling nodes
	if (is_sibs_null && is_r_sibs_null) {
		if (!(*sibs == *(root.sibs))) {
			return false;
		}
	}
	
	// If this point was reached then all previous nodes and structure was
	// identical
	return true;
}

// Takes a character as an argument and attempts to add it to the node it is called on
bool CTree::addChild(char ch) {
	CTree* node = new CTree(ch);
	bool result = this->addChild(node);
	if (!result) {
		delete node;
	}
	return result;
}

// Takes a tree pointer as an argument and attempts to add it to the node it is called on
bool CTree::addChild(CTree *root) {
	// Checks if the root passed in is truly the root of a tree
	// Then checks if the current node has a child, if not, its makes
	// the passed in root the child of the current node. Then checks if
	// the passed in root's value is less than the current node's child 
	// value, and if so, inserts the root as the direct child of the 
	// current node and the direct left sibling of the old child node
	if (root == nullptr || root->prev != nullptr || root->sibs != nullptr) {
		return false;
	} else if (kids == nullptr) {
		kids = root;
		kids->prev = this;
		return true;
	} else if (root->data < kids->data) {
		root->prev = this;
		root->sibs = kids;
		kids->prev = root;
		kids = root;
		return true;
	}

	// If code gets here, then there is a child node of the current node that
	// is less than the passed in root's value. So we attempt to add the 
	// passed in node as a sibling of the child node
	return this->kids->addSibling(root);
}

// Converts the tree into a string by recursively traversing depth-first
string CTree::toString() {
	// Stores the data value in the current node into a stringstream
	stringstream elements;
	elements << data << '\n';
	
	// Checks if the current node has a child, if so, recursively calls
	// toString on the child node and appends the result to the elements
	// stringstream
	if (kids != nullptr) {
		elements << kids->toString();
	}
	
	// Checks if the current node has a sibling, if so, recursively calls
	// toString on the sibling and appends the result to the elements stringstream
	if (sibs != nullptr) {
		elements << sibs->toString();
	}

	return elements.str();
}

// Attempts to add a new data value as a sibling by calling the other addSibling function
bool CTree::addSibling(char ch) {
	CTree* new_node = new CTree(ch);
	bool result = this->addSibling(new_node);
	// Check if the node was added, if not delete the created node
	if (!result) {
		delete new_node;
	}
	return result;
}

// Attempts to add the new root node as a sibling of the current node
bool CTree::addSibling(CTree *root) {
	// Checks if the node is not a valid root node AND if the current node is a root node. 
	// If either case is true, t simply returns false 
	if (root == nullptr || root->prev != nullptr || root->sibs != nullptr || prev == nullptr) {
		return false;
	}
	
	// Checks if the passed in root's value is less than the current node's data value
	// If so, the passed in value is inserted before the current node value.
	// Then checks if the data value is the same as the root's data value, if so
	// it simply returns false. Then checks if the current not does not have a
	// sibling, if true, then is makes the passed in root the current node's sibling
	// Finally, it calls the addSibling function on the current node's sibling if no
	// other conditions were satisfied	
	if (root->data < data) {
		prev->sibs = root;
		root->prev = prev;
		root->sibs = this;
		prev = root;
		return true;
	} else if (root->data == data) {
		return false;
	} else if (sibs == nullptr) { 
		sibs = root;
		sibs->prev = this;
		return true;
	} else {
		//should only run if data < root->data 
		return sibs->addSibling(root);
	}
}

// Overloads the << operator by calling the to string function and dumping the result into
// an out stream
ostream& operator<<(ostream& os, CTree& rt) {
	os << rt.toString();
	return os;
}
