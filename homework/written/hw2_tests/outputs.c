#include <stdio.h>
#include <stdbool.h>

int main(void) {
	int i = 1;
	while (i < 10); {
		if (i % 2 >= 0)
			printf("%d ", i++);
	}

	return(0);

}
