//Andres Recalde
//arecald1
#include <vector>
#include <string>
#include <map>

#ifndef LANGUAGE_MODELS_H
#define LANGUAGE_MODELS_H

//opens the command line argument file with the list of files
void open_list(const std::string filename_list, std::vector<std::string>& filenames, int& error_code);

//opens each file and extracts the text from it to put it in a vector
void read_text(const std::vector<std::string>& filenames, std::vector<std::string>& str_vec);

//loops through the previously made vector and creates a map that count the number of trigram occurances
void create_trigrams(const std::vector<std::string>& all_text, std::map<std::string, int>& trigram);

//prints the trigrams in ascending alphabetical order
void ascending(const std::map<std::string, int>& trigram);

//prints the trigrams in descending alphabetical order
void descending(const std::map<std::string, int>& trigram);

//prints the trigrams ordered from greatest to least occurances
void count_ordered(const std::map<std::string, int>& trigram);

//prints the full trigram with the most occurances that leads with the two strings inputted from the command line
void frequent(std::map<std::string, int>& trigram, const std::vector<std::string>& all_text, std::string x, std::string y);

//selects an operation to perform
void choose_op(std::string op, std::map<std::string, int>& trigram, const std::vector<std::string>& all_text, std::string x, std::string y);

#endif
