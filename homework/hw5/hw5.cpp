//Andres Recalde
//arecald1

#include <iostream>
#include <sstream>
#include <vector>
#include <map>
#include "language_model.h"

using std::cout; using std::cin; 
using std::cerr; using std::endl; 
using std::string;
using std::vector; using std::map;

int main(int argc, char ** argv) {
	//declare and initialize the value to be returned
	int error = 0;
	//initilize empty strings in case f operation is called
	string x = "";
	string y = "";

	//declare a vector that will hold all of the filenames
	vector<string> filenames;
	if (argc < 2) {
		cerr << "Invalid file: " << endl;
		error = 1;
		return error;
	}
	//Open the text file containing the list of filenames
	open_list(argv[1], filenames, error);
	if (error == 1) {
	   return error;
	}
		
	//pull all text from the files
	vector<string> all_text;
	read_text(filenames, all_text);

	//create a map of trigram and integer 
	map<string, int> trigram;
	create_trigrams(all_text, trigram);

	//store the operation in a string
	string op;
	if (argc > 2) {
	   op = argv[2];
	} else {
	    cerr << "Invalid command: valid options are a, d, c, and f" << endl;
	    error = 2;
	    return error;
	}

	//check if an operation is called and check for errors accordingly
	if (op.compare("a") == 0 || op.compare("d") == 0 || op.compare("c") == 0 || op.compare("f") == 0) {
		if (op.compare("f") == 0) {
		   if (argc < 5) {
		   	cerr << "Invalid argument list: f requires two additional command-line arguments" << endl;
			error = 3;
			return error;
		   }
		   //intialize strings will the correct command line arguments for operation f
		   x = argv[3];
		   y = argv[4];
		}
	} else {
	   cerr << "Invalid command: valid options are a, d, c, and f" << endl;	
	   error = 2; 
	   return error;
	}

	//call the correct operation
	choose_op(op, trigram, all_text, x, y);

	return error;
}
