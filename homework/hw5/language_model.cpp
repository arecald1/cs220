//Andres Recalde
//arecald1
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include "language_model.h"

using std::cout; using std::cin; 
using std::endl; using std::string;
using std::cerr; 
using std::vector; using std::map;

//opens the command line argument file with the list of files
void open_list(const string filename_list, vector<string>& filenames, int& error_code){
	//opens a filestream to read data from the filename provided
	std::ifstream inlist(filename_list);
	//checks if the file exists/opened successfully
	if (inlist.is_open()) { 
		//if opened successfully, create a string that will hold each text filename
		//then push the filename to the back of a string vector
		string file;
		while (inlist >> file) {
			filenames.push_back(file);
		}
	} else {
		//output error message and set error_code to proper value
	   	cerr << "Invalid file list: " << filename_list << endl;
		error_code = 1;
	}
}

//opens each file and extracts the text from it to put it in a vector
void read_text(const vector<string>& filenames, vector<string>& str_vec){
     for (vector<string>::const_iterator it = filenames.cbegin();
	  it != filenames.cend(); ++it) {   	     
	//open the file that is in the iterator of the filename list
	std::ifstream infile(*it);
        if (infile.is_open()) {
		string word;
		//Signify start of one file in string vector
		str_vec.push_back("<START_1>");
		str_vec.push_back("<START_2>");
		//Place every word from the text into the vector
		while(infile >> word) {
			str_vec.push_back(word);
		}
		//Signify the end of the text file
		str_vec.push_back("<END_1>");
		str_vec.push_back("<END_2>");
     	} else {
	    	cerr << "Invalid file: " << *it << endl;
	}
     }
}

//loops through the previously made vector and creates a map that count the number of trigram occurances
void create_trigrams(const vector<string>& all_text, map<string, int>& trigram) {
	//create the trigram key
	string word_key;
	//loop that counts each trigram using a map
	for (vector<string>::const_iterator it = all_text.cbegin();
	     it != all_text.cend(); ++it) {
	     if	(*it != "<START_1>" && *it != "<START_2>") {
		//creates trigram key based on iterator
		word_key = '[' + *(it - 2) + ' ' + *(it - 1) + ' ' + *it + ']';
		trigram[word_key]++;
	     }

	}
}

//prints the trigrams in ascending alphabetical order
void ascending(const map<string, int>& trigram) {
	//A for loop that prints out the map since they are stored in ascending order
	for (map<string, int>::const_iterator it = trigram.cbegin();
	     it != trigram.cend(); ++it) {
		cout << it->second << " - " << it->first << endl; 
	}

}

//prints the trigrams in descending alphabetical order
void descending(const map<string, int>& trigram) {
	//a for loop that prints out the reverse of the map since they are stored in ascending order
	for (map<string, int>::const_reverse_iterator it = trigram.crbegin();
	     it != trigram.crend(); ++it) {
	     cout << it->second << " - " << it->first << endl;
	}

}

//prints the trigrams ordered from greatest to least occurances
void count_ordered(const map<string, int>& trigram) {
	//initialize max_val which will be used for comparisons later
	int max_val = 0;
	//loop through entire trigram map and store the highest counted trigram value
	for (map<string, int>::const_iterator it = trigram.cbegin();
	     it != trigram.cend(); ++it) {
	     if (it->second > max_val) {
	     	max_val = it->second;
	     }
        }
	
	//then, simply loop for every value less than or equal to the max_val and greater than 0. 
	//Only print the trigrams that have a frequency of i. If there is no frequency of i, nothing prints,
	//if there is a trigram with frequency i, it will print and since maps are stored in ascending
	//alphabetical order, it will always print in ascending alphabetical order for tiebreakers
	for (int i = max_val; i > 0; i--) {
		for (map<string, int>::const_iterator it = trigram.cbegin(); it != trigram.cend(); ++it) {
			if (it->second == i) {
		   	   cout << it->second << " - " << it->first << endl;
			}
		}
	}


}

//prints the full trigram with the most occurances that leads with the two strings inputted from the command line
void frequent(map<string, int>& trigram, const vector<string>& all_text, string x, string y) {
	//declare the values used later for comparison
	int freq_num = 0;
	string key;
	string freq_key;
	string comp;

	//loop through the original string vector created earlier
	for (vector<string>::const_iterator it = all_text.cbegin();
	     it != all_text.cend(); ++it) {
	    //check that it is not a <START> string so that the iterator stays in bounds	
	    if (*it != "<START_1>" && *it != "<START_2>") {
		//then ensure that the two strings before the iterator are the two string
		//specified in the command line, if so create a key with the full trigram    
	    	if(x.compare(*(it - 2)) == 0 && y.compare(*(it - 1)) == 0) {
	    	   key = '[' + *(it - 2) + ' ' + *(it - 1) + ' ' + *it + ']';

		   //compare the trigram value with the most frequently occuring trigram's value so far
		   //if the new value is larger, then save the key and frequency of trigram for later comparison
		   //or printing. If the values are the same, compare the strings and keep the trigram that
		   //occurs earliest in the ASCII table
		   if(trigram[key] > freq_num) {
		   	freq_num = trigram[key];
		   	freq_key = key;
			comp = *it;
		   } else if (trigram[key] == freq_num) {
		   	int res = comp.compare(*it);
			if (res > 0) {
			   freq_key = key;
			   comp = *it;  
			}
		   }
		}
	    }
         }
	
	//check if any matches were found and print the appropriate message
	if (freq_num == 0) {
		cout << "No trigrams of the form [" << x << ' ' << y << " ?] appear" << endl;
	} else {
		cout << trigram[freq_key] << " - " << freq_key << endl;
	}
}

//selects an operation to perform
void choose_op(string op, map<string, int>& trigram, const vector<string>& all_text, string x, string y) {
	
    if (op.compare("a") == 0) {
	    ascending(trigram);
    } else if (op.compare("d") == 0) {
	    descending(trigram);
    } else if (op.compare("c") == 0) {
	    count_ordered(trigram);
    } else if(op.compare("f") == 0) {
	    frequent(trigram, all_text, x, y);	
    } 
}
