//Andres Recalde
//arecald1

#include <stdio.h>
#include "dnasearch.h"
#include <ctype.h>

int check_file (FILE* file) {
	char element = '5'; 
	int num_elements = 0;
	
	while(fscanf(file, "%c", &element) == 1) {
		// if it is not a whitespace, this checks if it is a valid char value and increments a counter
		if (isspace(element) == 0) {
			switch (element) {
				case 'A':
				case 'a': num_elements++;
				 	  break;
				case 'C':
				case 'c': num_elements++;
				  	  break;
				case 'G':
				case 'g': num_elements++;
					  break;
				case 'T':
				case 't': num_elements++;
					  break;
				default: num_elements = 15001; //triggers error
					 break;
			} //end of switch

			if (num_elements > 15000) { //detects error and stops further unnecessary loops
				break;
			}
		} //end of wrapped if statement

	} //end of while loop

	if (num_elements <= 0 || num_elements > 15000) { //checks if error is met
		return -1;
	}

	return num_elements; //assuming you'll need the number of elements for later.
}

int uppercase(char string[], int size) {
	for (int i = 0; i < size; i++) { //iterates over every char in the string
		
		if (isspace(string[i]) == 0 && string[i] > 90){ //checks if not a space and if lowercase
		string[i] = string[i] - 32; //converts lowercase to uppercase
		}
	}
	return 0;
} 


int file_to_string(FILE* file, char t_string[]) { 
	char element = '5';
	int i = 0;
	
	rewind(file); //resets file pointer to beginning
	while(fscanf(file, "%c", &element) == 1) { //checks that elements are available to scan
		
		//makes sure the element is not a space char,
		//then assigns the element from the file to the array position
		if (isspace(element) == 0) {
		t_string[i] = element; 
		i++; 
		}
	
	}
	t_string[i] = '\0'; //sets last index to null terminator
	return 0;

}

int check_input(char in[], int size, int num_elements) { 
	if (size > num_elements) { //checks if pattern size is larger than the text string
		return -2;
	}
	for(int j = 0; j < size; j++) { //iterates over every element of the pattern
		switch(in[j]) { 	//checks if the character is valid
			case 'A':
			case 'C':
			case 'G':
			case 'T': 
				 break;
			default: return -1;
		
		}

	}
	

	return 0;	

}

int pattern_match(const char t[], int tlen, const char p[], int plen, int start_at) {
	int offset = 0;
	int counter = 0;
	for (int i = start_at; i <= tlen - plen; i++) {
		offset = i;
		counter = 0;
		for (int j = 0; j < plen; j++) {
			if (p[j] == t[j + i]) {
				counter++;
			}
		}
		if (counter == plen) {
			return offset;
		}
	}

	return -1;

}






