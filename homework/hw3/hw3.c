//Andres Recalde
//arecald1
#include <stdio.h>
#include <string.h>
#include "dnasearch.h"


int main(int arg_num, char* argv[]) {
	
	int num_elements = -2;
	char text_string[15001];
	char input[15001];
	int in_err = -5;

	if (arg_num != 2) { //checks for correct number of command line arguments
		return 3;
	}

	//opens file with name under argv[1] for reading
	//argv[1] because argv[0] is executable name (./a.out)
	FILE* t = fopen(argv[1], "r");	
	
	//checks if file is valid, check_check returns num elements if valid,
	//if invalid returns -1
	//If -1 is returned, error is triggered an 1 is returned from main
	num_elements = check_file(t); 
	if (num_elements == -1) {
		printf("Invalid text\n");
		fclose(t);
		return 1;
	}
	
	//Converts file to a string, closes file pointer, and capitalizes the string
	file_to_string(t, text_string);
	fclose(t);
	uppercase(text_string, num_elements);

	int i = 0;
	int offset = -2; //could just make this -1
	int counter = 0;
	int len_input = -1;
	
	//takes input from user and displays offsets where pattern occurs in text
	//if Invalid pattern, main returns 2
	while(scanf(" %s", input) == 1) {
		counter = 0;
		
		//makes string uppercase and checks its valid	
		len_input = (int)strlen(input); 
		uppercase(input, len_input);
		in_err = check_input(input, len_input, num_elements); //returns -1 if not valid char, -2 if too large
		
		if(in_err != 0) {
			printf("Invalid pattern\n");
			return 2;
		}		
		
		i = 0;
		offset = -2;

		//Displays offsets of pattern in text or "Not found" if none exist
		printf("%s ", input);	
		while(i <= num_elements - len_input && offset != -1) {
			offset = pattern_match(text_string, num_elements, input, len_input, i);
			
			if (offset != -1) {
				printf("%d ", offset);
				counter++;
			}
			
			i = offset + 1;
		}
		if (counter == 0) {
		printf("Not found");
		}

		printf("\n");	
	}



return 0;
}
