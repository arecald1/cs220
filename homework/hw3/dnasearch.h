//Andres Recalde
//arecald1

int check_file(FILE* file);

int file_to_string(FILE* file, char t_string[]);

int uppercase(char string[], int size);

int check_input(char in[], int size, int num_elements);

int pattern_match(const char t[], int tlen, const char p[], int plen, int start_at);
