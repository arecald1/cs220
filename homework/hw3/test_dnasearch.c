//Andres Recalde
//arecald1

#include <stdio.h>
#include "dnasearch.h"
#include <assert.h>
#include <string.h>

int fileeq(char lhsName[], char rhsName[]);

int main(void) {

FILE* mixed_case_no_space = fopen("mixed.txt", "r");
assert(9 == check_file(mixed_case_no_space));

FILE* digit_in_file = fopen("wrong_char_digit.txt", "r");
assert(-1 == check_file(digit_in_file));

FILE* invalid_char = fopen("wrong_char.txt", "r");
assert(-1 == check_file(invalid_char));

FILE* space_char = fopen("spaces_between.txt", "r");
assert(7 == check_file(space_char));

FILE* empty = fopen("empty_file.txt", "r");
assert(-1 == check_file(empty));

fclose(mixed_case_no_space);
fclose(digit_in_file);
fclose(invalid_char);
fclose(space_char);
fclose(empty);


//file_to_string tests:
//Testing that a file with mixed cases and spaces will convert to string
FILE* test_mixed_space = fopen("spaces_between_mixed.txt", "r");
char mixed_space[15001];
file_to_string(test_mixed_space, mixed_space);
assert(0 == strcmp("cgtAtgCtAGtcAATct", mixed_space));
fclose(test_mixed_space);

FILE* upper_space = fopen("upper.txt", "r");
char upper_case[15001];
file_to_string(upper_space, upper_case);
assert(0 == strcmp("GCATGCATGCATGCATGCAT", upper_case));
fclose(upper_space);

FILE* lower_space = fopen("lower.txt", "r");
char lower_case[15001];
file_to_string(lower_space, lower_case);
assert(0 == strcmp("tagcgttaggattacg", lower_case));
fclose(lower_space);

FILE* no_space = fopen("nospace.txt", "r");
char no_space_str[15001];
file_to_string(no_space, no_space_str);
assert(0 == strcmp("acgctga", no_space_str));
fclose(no_space);

//tests for check_input function
//1 - checks that a pattern less than the length of string is valid
//2 - checks that a pattern greater than length of string is invalid
//3 - checks that a  pattern with an invalid character is invalid
//4 - checks that an edgecase pattern is valid

assert(0 == check_input("GATC", 4, 8)); //1
assert(-2 == check_input("GCTAA", 5, 4)); //2
assert(-1 == check_input("GBT", 3, 12)); //3
assert(0 == check_input("GCTAAGT", 7, 7)); //4

//uppercase function tests:

//test mixed case to all uppercase
char mixed[] = "ACgTttGACCtagC";
uppercase(mixed, strlen(mixed));
assert(0 == strcmp("ACGTTTGACCTAGC", mixed));

//test all uppercase
char upper[] = "AGCTCAGCTAGC";
uppercase(upper, strlen(upper));
assert(0 == strcmp("AGCTCAGCTAGC", upper));

//test all lowercase
char lower[] = "acgctcgatcga";
uppercase(lower, strlen(lower));
assert(0 == strcmp("ACGCTCGATCGA", lower));

//Tests for pattern_match below

//Conditions:
//All patterns are less than length of string and there is a match
//1 - match is in front (starting index starts match)
//2 - match is not in front (start_at is not on beginning of a match)
//3 - starting index is on a match and not in front
//4 - starting index is after first match and before second match
//5 - starting index is after first match and the last acceptable start_at (difference betweeen
//length of first string and length of pattern)

assert(0 == pattern_match("CGATGACCGA", 9, "CGA", 3, 0)); //1
assert(2 == pattern_match("ATCGACCGA", 9, "CGA", 3, 1)); //2
assert(2 == pattern_match("ATCGACCGA", 9, "CGA", 3, 2)); //3
assert(6 == pattern_match("ATCGACCGA", 9, "CGA", 3, 3)); //4
assert(6 == pattern_match("ATCGACCGA", 9, "CGA", 3, 6)); //5

//Conditions:
//Match is overlapping
//1 - match is found at first index of string
//2 - match is at 2 index start_at is not on a match (1)
//3 - match is at 2 index, start_at on match and not at first index of string
//4 - match is on 1 and pattern is length 2, start_at is first index
//5 - match is on 1, pattern is length 2, start_at is on match index
//6 - match is on 3, pattern is length 2, start_at is between first and second match
//7 - match is on 3, pattern is length 2, start_at is on second match and max start_at value (difference
// in length of string and pattern); 

assert(0 == pattern_match("CTCTC", 5, "CTC", 3, 0)); //1
assert(2 == pattern_match("CTCTC", 5, "CTC", 3, 1)); //2
assert(2 == pattern_match("CTCTC", 5, "CTC", 3, 2)); //3
assert(1 == pattern_match("CTCTC", 5, "TC", 2, 0)); //4
assert(1 == pattern_match("CTCTC", 5, "TC", 2, 1)); //5
assert(3 == pattern_match("CTCTC", 5, "TC", 2, 2)); //6
assert(3 == pattern_match("CTCTC", 5, "TC", 2, 3)); //7

//Conditions:
//String is normal length, pattern is one charcter
//General test to see if every character will be reported assuming
//the indexes are between or on a match

assert(1 == pattern_match("CTCTC", 5, "T", 1, 0));
assert(1 == pattern_match("CTCTC", 5, "T", 1, 1));
assert(3 == pattern_match("CTCTC", 5, "T", 1, 2));
assert(3 == pattern_match("CTCTC", 5, "T", 1, 3));
assert(0 == pattern_match("GAGAG", 5, "G", 1, 0));
assert(2 == pattern_match("GAGAG", 5, "G", 1, 1));
assert(4 == pattern_match("GAGAG", 5, "G", 1, 4));

//Conditions:
//String and pattern are same size and match

assert(0 == pattern_match("AGTCCGAT", 8, "AGTCCGAT", 8, 0));

//Conditions:
//Pattern does not occur in string at all or after start_at
//1 - simple pattern is not in string
//2 - pattern is character and not in string
//3 - pattern is same size as string
//4 - pattern happens before start_at but not after

assert(-1 == pattern_match("AGCTCGCTA", 9, "GAT", 3, 0));
assert(-1 == pattern_match("CGCTCGCTT", 9, "A", 1, 0));
assert(-1 == pattern_match("AGCTCGCTA", 9, "GATCAGTCA", 9, 0));
assert(-1 == pattern_match("AGCTCGCTA", 9, "GC", 2, 6));

//Tests for output:
//Tests for output when file is not valid
assert(1 == fileeq("invalid_file_output.txt","invalid_file_expected.txt"));
//Tests for output when pattern is not valid but patterns before it are valid
assert(1 == fileeq("invalid_pattern_output.txt","invalid_pattern_expected.txt"));
//Tests for simple valid patterns where some appear in string and others do not
assert(1 == fileeq("pattern_offset_mixed.txt","pattern_offset_mixed_expected.txt"));
//Tests for a when an invalid pattern occurs and then valid patterns afterwards do not execute
assert(1 == fileeq("invalid_pattern_then_valid.txt","invalid_pattern_then_valid_exp.txt"));



printf("All tests passed!\n");
return 0;
}




/*
 * Reads lhs and rhs character by character until either reaches eof.
 * Returns true if the files can be read and the two files match
 * character by character. Returns false if two files either can't be
 * opened or don't match.
 */
int fileeq(char lhsName[], char rhsName[]) {
	    FILE* lhs = fopen(lhsName, "r");
	    FILE* rhs = fopen(rhsName, "r");

	    // don't compare if we can't open the files
	    if (lhs == NULL || rhs == NULL) return 0;
		    
	    int match = 1;
	    // read until both of the files are done or there is a mismatch
	    while (!feof(lhs) || !feof(rhs)) {
	        if (feof(lhs) ||                  // lhs done first
	            feof(rhs) ||                  // rhs done first
	            (fgetc(lhs) != fgetc(rhs))) { // chars don't match
	            match = 0;
	            break;
	         }
	    }
	    fclose(lhs);
	    fclose(rhs);
	    return match;
}                                                                                                
