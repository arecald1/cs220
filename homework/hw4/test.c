
/**
#include <stdio.h>
#include <stdlib.h>
     int h(const char *s, int *a) {
	while (*s) {
		a[*s++]++;
	}
      }
int main(void) {
	const char *t = "colorless green ideas";
	int *x = (int *) calloc(128, sizeof(int));
	h(t, x);
	for (int i = 'a'; i <= 'z'; i++) {
		if (x[i] > 0) {
			printf("%c: %d\n", i, x[i]);
		}
	}
	free(x);
	return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *x(const char *s) {
	int len = (int) strlen(s);
	char *buf = (char *) malloc(len*2 + 1);
	strcpy(buf, s);
	strcat(buf, s);
	for (int i = 0; i < len; i++) {
		  char *p = buf + i*2;
		    char *q = buf + i*2 + 1;
		      char t = *p;
		        *p = *q;
			  *q = t;
	}
	return buf;

}
int main(void) {
	const char *strs[] = {"emu", "ostrich", "cassowary", "kiwi"};
	for (int i = 0; i < 4; i++) {
		char *p = x(*(strs + i));
		printf("%s\n", p);
	}
	return 0;
}

#include <stdio.h>

int main(void) {
int i = 6;
//int *ip;
int *iq = &i;
int **ipp;
ipp = &iq;

printf("%d\n", **ipp);

return 0;
}

#include <stdio.h>
#include <stdlib.h>
struct data {
	char *str;
	double *vals;
	int n;
};

int main(void) {
int sz = 6;
struct data *d = (struct data*) malloc(sizeof(struct data));
d->str = (char*) malloc(sz * sizeof(char) + 1);
(*d).vals = (double*) malloc(sz * sizeof(double));
d->n = sz;
// (code using d)
 free(d);

return 0;
}
****/

#include <stdio.h>

int main(void) {
	int array[3][4] = { {1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12} };
	**array = 20;
	int *p = array[1];
	*(p+1) = 30;
	*(p+5) = 40;
	p += 3;
	p[2] *= 10;
	*(p-(*p-1)) *= 100;
	for (int i=0; i<3; i++) {
		for (int j=0; j<4; j++) {
			printf("%d ", array[i][j]);
		}
		printf("\n");
	}

	return 0;
}
