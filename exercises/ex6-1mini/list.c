#include "list.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

// put char into a newly created node
Node * create_node(char ch) {
  Node * node = (Node *) malloc(sizeof(Node));
  assert(node); //confirm malloc didn't fail

  node->data = ch;
  node->next = NULL;
  return node;
}

// output the list iteratively
void print(const Node * cur) {
  while (cur != NULL) {
    printf("%c ", cur->data);
    cur = cur->next;  // advance to next node
  }
}

int length(const Node *n) {
	int count = 0;
	while(n != NULL) {
		count++;
		n = n->next;
	}
	return count;

}

void add_after(Node * n, char val) {
	Node * node = (Node *) malloc(sizeof(Node));
	assert(node);

			
	node->data = val;
	node->next = n->next;
	n->next = node;
}

void reverse_print(const Node * head) {
	
	if(head->next == NULL) {
		printf("%c ", head->data);
	} else {
		reverse_print(head->next);
		printf("%c ", head->data);
	}


}
