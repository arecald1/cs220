nclude <iostream>
#include <vector>
using std::cout; using std::endl; using std::vector;
 
bool has_dups(vector<char> vec) {
	  // complete the function to return true if
	  // vec has duplicates, false otherwise
	  //vector<char> comp;
	  //for (vector<char>::iterator it = vec.begin(); it != vec.end(); ++it) {
	  //	comp.push_back(*it);
	  //}

	  bool result = false;
	  for (vector<char>::iterator out = vec.begin(); out != vec.end(); ++out) {
	  	for (vector<char>::iterator check = out + 1; check != vec.end(); ++check) {
			if (*out == *check) {
				result = true;
			}
		}
	  }

	   return result; 
	  }
	       
	       int main() {
	       vector<char> v;
	       for (char c = 'a'; c < 'i'; c++) v.push_back(c);
	       v.insert(v.begin() + 2, 'f');
	       cout << "v's content:" << endl;
	       for (char c: v) cout << c << " ";
	       cout << endl;
	       cout << "does v have duplicates? " << has_dups(v) << endl;
	        return 0;
	        }
