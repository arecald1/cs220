#include <stdio.h>
#include "funs.h"


int abs(int x) {
	if (x < 0) {
		return x * -1;
	} else {
		return x;
	}

}
